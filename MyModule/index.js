var mysql = require('mysql');
// noinspection JSAnnotator
var client = mysql.createConnection({
    host     : '127.0.0.1',
    port     : '3300',
    user     : 'enet',
    password : 'enet750',
    database : 'enet'
});



/*module.exports.mygetperson = function(par1,par2){
    return new Promise((resolve, reject) => {
        var tt = client.query('call wadGetPerson(' + par1 + ')', function (error, result, fields) {

            if (error) {
                return reject(error);
            }
            else {
                return resolve(result[0]);
            }

        })
    })
};*/

function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
};

module.exports.getTeamIds = function(objPlayers,objTeamName,arrTeamId,arrTransferPlayers ){
    var ObjStr = '';
    var objTeamName1 = [];
    var arrTeamId1 = [];

    objPlayers.forEach(function (item) {

        if(item.mainTeamId != null) {
            arrTeamId1.push(item.mainTeamId);
            arrTransferPlayers.push(item.id);
        }

        ObjStr = item.mainTeamName;
        if(ObjStr != null) {
            while (ObjStr.indexOf(".") > -1) {
                ObjStr = ObjStr.replace(".", "");
            }
            while (ObjStr.indexOf("ø") > -1) {
                ObjStr = ObjStr.replace("ø", "oe");
            }
            objTeamName1.push('"' + ObjStr + '"');
        }
    })

    objTeamName1 = objTeamName1.filter(onlyUnique);
    objTeamName.push(objTeamName1);

    arrTeamId1 = arrTeamId1.filter(onlyUnique);
    arrTeamId.push(arrTeamId1);

    return  1;
};

module.exports.mygetEvent = function(objTeamName,arrTeamId,objIncendent){
    return new Promise((resolve, reject) => {

        var sqld = "";
        //if(objIncendent.length > 0){
          //  sqld = "and i.elapsed in (" + objIncendent + ")";
        //};
        var sql = `select DISTINCT
                        e.id as event_id ,
                        e.startDate as event_Date,
                        e.\`name\` as event_name,
                        t.id as tournament_id,
                        t.\`name\` as tournament_name,
                        #ep.id as event_participant_id,
                        #i.id as incident_id,
                        #i.incident_code as incident_code,
                        ts.id as tournament_stage_id
                    from event e 
                    inner join event_participant ep on ep.eventFK = e.id 
	                    and ep.participantFK in (` + arrTeamId + `) 
                    inner join tournament_stage ts on ts.id = e.tournament_stageFK
                    inner join tournament t on t.id = ts.tournamentFK
                    inner join tournament_template tt on tt.id = t.tournament_templateFK
                    INNER JOIN incident i on i.event_participantsFK = ep.id
                    and i.incident_code = "goal" `+ sqld +
                    ` where 
                    tt.id not in(489, 344, 293, 114) 
                    #AND e.startdate between "2018-01-01" AND "2019-01-01"
                    AND (e.startdate between STR_TO_DATE(CONCAT('01/01/',CAST((YEAR(NOW())-1) AS char)),'%d/%m/%Y') 
                            AND STR_TO_DATE(CONCAT('01/01/',CAST((YEAR(NOW())+1) AS char)),'%d/%m/%Y'))
                    AND (INSTR(e.\`name\`,` + objTeamName[0] + `) and INSTR(e.\`name\`,` + objTeamName[1] + `))
                    order by e.startdate desc`;

        //(objIncendent.length > 0) &&

        //console.log(objTeamName[2]);

        if ((objTeamName[1]) && (arrTeamId != 0)){
            var tt = client.query(sql, function (error, result, fields) {

                if (error) {
                    return reject(error);
                }
                else {
                    return resolve(result);
                }

            })
        }
        else {
            return resolve(0)
        }

    })
};

module.exports.mygettournamentStage = function(par1){
    return new Promise((resolve, reject) => {

        if(par1 != 0) {
            var p1;
            par1.forEach(function (item) {
                p1 = item.tournament_stage_id;
            });


            var tt = client.query('call getTournamentById(1,' + p1 + ')', function (error, result, fields) {

                if (error) {
                    return reject(error);
                }
                else {
                    return resolve(result[0]);
                }

            })
        }
        else{
            return resolve(0);
        }
    })
};

module.exports.mygetplayerfull = function(par1){
    return new Promise((resolve, reject) => {

        var tt = client.query('call getPersonByParticipants(' + par1 + ')', function (error, result, fields) {

            if (error) {
                return reject(error);
            }
            else {
                return resolve(result[0]);
            }

        })
    })
};

module.exports.getNamePerson = function (strName,objIncendent,obj,relevance) {
    strName = '"';
    obj.forEach(function(item){
        if((item.type=="Person")&&(item.relevance >= relevance)) {
            if(strName != '"'){
                strName += ',';
            };
            strName += item.text;
            if(item.hasOwnProperty("disambiguation")){
                if(item.disambiguation.hasOwnProperty("name")) {
                    strName += ',' + item.disambiguation.name;
                };
            };
        };

        if((item.type=="Quantity")&&(item.relevance >= relevance)){

            var num = parseInt(item.text.replace(/\D+/g,""));
            if(num) {
                objIncendent.push(num);
            }
           // console.log(item.text.substr(0,2));
        };
    });
    strName += '"';
    return strName;
};

module.exports.getNameTeam = function (strName,obj,relevance) {
    strName = '"';
    obj.forEach(function(item){
        if(item.relevance >= relevance) {
            if(strName != '"'){
                strName += ',';
            };
            strName += item.text;
        };
    });
    strName += '"';
    return strName;
};



module.exports.mygetTeamFull = function(par1){
    return new Promise((resolve, reject) => {
        //console.log(par1);
        var query = `SELECT DISTINCT p.*
                                FROM
	                            participant AS p,
	                            ( SELECT DISTINCT l.objectFK AS objectFK
		                          FROM language AS l
		                          WHERE FIND_IN_SET(l.name,`+par1+`)
		                          and l.object = "participant"
		                          ) AS k
                                WHERE p.id = k.objectFK
                                and p.enetSportID = "s"
                                and p.type = "team"`;
        var tt = client.query(query, function (error, result, fields) {

            if (error) {
                return reject(error);
            }
            else {
                return resolve(result);
            }

        })
    })
};

module.exports.mygetTransferFull = function(par1){
    return new Promise((resolve, reject) => {
        //console.log(par1);
        var query = `select 
                        t.id as id,
                        t.amount_description,
                        t.amount as amount,
                        t.last_update as date,
                        t.transfer_type as transferType,
                        t.currency as currency,
                        p.name as participantName,
                        p.id as participantId,
                        pTeamFrom.name as teamFrom,
                        pTeamFrom.id as teamFromId,
                        pTeamTo.name as teamTo,
                        pTeamTo.id as teamToId,
                        t.rumour_source as source,
                        t.rumour_probability as probability,
                        t.is_rumour as isRumour,
                        "" as tournamentParticipant,
                        t.date_from as dateFrom,
                        t.date_to as dateTo,
                        ifnull(lFrom.name, cFrom.name) as countryFrom,
                        ifnull(lTo.name, cTo.name) as countryTo
                    from rlz_transfer t
                        inner join participant p on p.id = t.participantFK
                        inner join participant pTeamFrom on pTeamFrom.id = t.participantFromFK
                        inner join participant pTeamTo on pTeamTo.id = t.participantToFK
                        inner join country cFrom on cFrom.id = pTeamFrom.countryFK
                        left join language lFrom on lFrom.object = 'country' and lFrom.objectFK = cFrom.id and lFrom.language_typeFK = 2
                        inner join country cTo on cTo.id = pTeamTo.countryFK
                        left join language lTo on lTo.object = 'country' and lTo.objectFK = cTo.id and lTo.language_typeFK = 2
                    where p.id in (`+par1+`) and t.is_rumour = false
                    and t.last_update >= IF(t.is_rumour = 1,(NOW() - INTERVAL 1 MONTH),'2013-01-01 00:00:00')
                    order by t.last_update desc;`;
        var tt = client.query(query, function (error, result, fields) {

            if (error) {
                return reject(error);
            }
            else {
                return resolve(result);
            }

        })
    })
};






