var express = require('express');
var franc =  require ( 'franc' );
var router = express.Router();


const bodyParser = require("body-parser");
const { check, validationResult } = require('express-validator/check');
const urlencodedParser = bodyParser.urlencoded({extended: false});
const translate = require('google-translate-api');


 var MyModule = require('../MyModule');



var mysql = require('mysql');
// noinspection JSAnnotator
var client = mysql.createConnection({
    host     : '127.0.0.1',
    port     : '3300',
    user     : 'enet',
    password : 'enet750',
    database : 'enet'
});


var  PersonStr = ''; //парамеер для запроса к базе
var  TeamStr   = '';
var  SportStr = '';
var  CategiriesStr = '';


var countentities = 20; //count entities
var countkeywords = 2; //count keywords
var intext = '';       //исходный текст
var outtext = '';      //переведенный текст
var inurl = '';        //адрес сайта для анализа
var relevance = 0.3;   //минмиальное значение relevance
var pjson = null;
var objJSON = {};

//Подключение к сервису анализа
var NaturalLanguageUnderstandingV1 = require('watson-developer-cloud/natural-language-understanding/v1.js');
var natural_language_understanding = new NaturalLanguageUnderstandingV1({
    'username': '8a45bb1c-16a4-4628-9746-096cb92413e1',
    'password': 'zWetmh0vxMIu',
    'version': '2018-03-19',
    'url': 'https://gateway.watsonplatform.net/natural-language-understanding/api/v1/analyze?version=2018-03-19'
});

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express',
                        pinurl: inurl,
                        pcountentities: countentities,
                        pcountkeywords: countkeywords,
                        prelevance: relevance,
                        ppch2: pjson});
});


//Основной обработчик запроса анализа  urlencodedParser,
router.post("/register", [check('pch2').isBoolean()], function (request, response) {
    if(!request.body) return response.sendStatus(400);

    const errors = validationResult(request);
    if(errors.isEmpty()){
        pjson = 'checked';
    }
    else{
        pjson = null;
    };

    //получаем входной текст или URL
    intext = `${request.body.t1}`;
    inurl  = `${request.body.purl}`;



    //Заменяем Датские сиволы
    //intext = intext.replace(/ø/g,"o");

    //Получаем переменные для параметров анализа
    countentities = parseInt(`${request.body.txt}`); //count entities
    countkeywords = parseInt(`${request.body.txt1}`); //count keywords
    relevance = parseFloat(`${request.body.prelevance}`);


    //Переводим на английский
    translate(intext, {to: 'en'}).then(ress => {
        outtext = ress.text;

        //Устанавливаем параметры анализа
        if(inurl==""){
            var parameters = {
                'text': outtext,
                //'url': "http://english-4life.com.ua/biografia/footbolist/Ronaldo.html",
                'features': {
                    //'relations': {},
                    //'categories': {},
                    //'concepts': {
                    //    "limit": 50
                    //},
                    'entities': {
                        'emotion': true,
                        'sentiment': true,
                        'limit': countentities
                    },
                    'keywords': {
                        'emotion': true,
                        'sentiment': true,
                        'limit': countkeywords
                    }
                }//,
                //'language':'en'
            }
        }
        else {
            var parameters = {
                //'text': outtext,
                'url': inurl,
                'features': {
                    //'relations': {},
                    //'categories': {},
                    'entities': {
                        'emotion': true,
                        'sentiment': true,
                        'limit': countentities
                    },
                    'keywords': {
                        'emotion': true,
                        'sentiment': true,
                        'limit': countkeywords
                    }
                }//,
                //'language': 'en'
            }
        }

        //Вызываем обработчик анализа текста
        natural_language_understanding.analyze(parameters, function(err, resp) {
            if (!err) {
                
               var objEntities = resp.entities;
               var objKeywords = resp.keywords;
               
            //Формируем строку параметров для запроса в базу

                var objIncendent = [];
                PersonStr = MyModule.getNamePerson(PersonStr,objIncendent,objEntities,relevance);

                //TeamStr = MyModule.getNameTeam(TeamStr,objKeywords,relevance);
                //console.log(PersonStr);


              //Получаем данные из базы по персоналиям
               var zzz={};

                    var objPlayers = {};


                    objPlayers = MyModule.mygetplayerfull(PersonStr).then(function (datap) {
                        objPlayers['PlayersFull:'] = datap;

                        var objTeamName = [];
                        var arrTeamId = [];
                        var arrTransferPlayers = [];
                        x = MyModule.getTeamIds(objPlayers['PlayersFull:'],objTeamName,arrTeamId,arrTransferPlayers);
                        objTeamName = objTeamName[0];
                        arrTeamId = arrTeamId[0];

                        //console.log(objTeamName);
                        //console.log(arrTeamId);
                        //console.log(objIncendent);
                        //console.log(arrTransferPlayers);


                        //objTeams = MyModule.mygetTeamFull(TeamStr).then(function (datateam) {
                        objEvent = MyModule.mygetEvent(objTeamName,arrTeamId,objIncendent).then(function (datateam) {
                            objEvent['Event:'] = datateam;

                        objTournamentStage = MyModule.mygettournamentStage(objEvent['Event:']).then(function (datateamt) {
                            objTournamentStage['TournamentStage:'] = datateamt;

                         objTransfer = MyModule.mygetTransferFull(arrTransferPlayers).then(function (datateamtr) {
                             objTransfer['Transfer:'] = datateamtr;

                           if(pjson != 'checked') {
                               response.render('index', {
                                   intxt: intext,
                                   pinurl: inurl,
                                   pcountentities: countentities,
                                   pcountkeywords: countkeywords,
                                   prelevance: relevance,
                                   ppch2: pjson,
                                   outjson: JSON.stringify(resp, null, 3),
                                   //keywords: JSON.stringify(zzz, null, 3),
                                   playerfull: JSON.stringify(objPlayers, null, 3),
                                   event: JSON.stringify(objEvent, null, 3),
                                   tournamentStage: JSON.stringify(objTournamentStage, null, 3),
                                   transfer: JSON.stringify(objTransfer, null, 3)
                               });
                           }
                           else{
                               objJSON['PlayersFull'] = datap;
                               objJSON['Event'] = datateam;
                               objJSON['TournamentStage'] = datateamt;
                               objJSON['Transfer'] = datateamtr;
                               response.end(JSON.stringify(objJSON,null,3));
                           };

                         });
                        });
                        });
                    });

            }
            else {
                console.log('error:', err);
            }
        });
        
    }).catch(err => {
        console.error(err);
    });
});


module.exports = router;
