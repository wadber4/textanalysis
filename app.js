var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();


//var NaturalLanguageUnderstandingV1 = require('watson-developer-cloud/natural-language-understanding/v1.js');
//var natural_language_understanding = new NaturalLanguageUnderstandingV1({
//    'username': '8a45bb1c-16a4-4628-9746-096cb92413e1',
//    'password': 'zWetmh0vxMIu',
//    'version': '2018-03-19',
//    'url': 'https://gateway.watsonplatform.net/natural-language-understanding/api/v1/analyze?version=2018-03-19'
//});

//const bodyParser = require("body-parser");
//const urlencodedParser = bodyParser.urlencoded({extended: false});

//var p1 = true;
//var p2 = true;
//var p3 = 5;
//var p4 = 2;
//var ptext = '';
//var ptext1 = '';

//const translate = require('google-translate-api');


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
